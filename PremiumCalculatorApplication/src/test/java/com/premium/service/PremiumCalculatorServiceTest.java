package com.premium.service;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
public class PremiumCalculatorServiceTest {

	private Customer customer;
	@Autowired
	private PremiumCalculatorService premiumCalculatorService;
	
	@Before 
	   public void setUp() {
	      customer = getData();
	  }
	
	@Test
	public void testPremiumCalculator() {
		double amount = premiumCalculatorService.getPremiumAmount(customer); 
		assertTrue(String.valueOf(amount).startsWith("6856"));
	}
	
	private Customer getData() {
		Customer customer = new Customer();
		Person p = new Person();
		p.setName("test");
		p.setAge(34);
		p.setGender("Male");
		p.setHyperTension(false);
		p.setBloodpressure(false);
		p.setSugar(false);
		p.setOverweight(true);
		p.setSmoking(false);
		p.setAlochol(true);
		p.setExercise(true);
		p.setDrugs(false);
		
		customer.setPerson(p);
		customer.setAmount(5000);
		return customer;
	}
}
