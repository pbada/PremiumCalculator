package com.premium.service;

import org.springframework.stereotype.Component;

@Component
public class PremiumCalculatorService {
	private double premium;
	private double basepremium=0;
	
	public double getPremiumAmount(Customer c){
		this.premium = c.getAmount();
		
		getPremiumByAge(c.getPerson().getAge());
		getPremiumByGender(c.getPerson().getGender());
		getPremiumByPreExiConditions(c.getPerson());
		getPremiumByHabits(c.getPerson());
		
		return premium;
	}
	public double getPremiumByAge(int age){
		if(age>=18 && age<=25){
			premium=premium+(premium *0.1);
		}else if(age>25 && age<=30){
			premium=premium+(premium *0.1);
			premium=premium+(premium *0.1);
		}else if(age>30 && age<=35){
			premium=premium+(premium *0.1);
			premium=premium+(premium *0.1);
			premium=premium+(premium *0.1);
		}else if(age>40){
			premium=premium+(premium *0.2);
		}
		basepremium=premium;
		return premium;//6655
	}
	private double getPremiumByGender(String gender){
		
		if(gender.equals("Male")){
			premium=premium+(premium *0.02);
			
		}		
		return premium;		
	}
	
	private double getPremiumByPreExiConditions(Person p){
		
		if(p.isHyperTension()){
			premium=premium+(premium *0.01);			
		}
		if(p.isBloodpressure()){
			premium=premium+(premium *0.01);			
		}
		if(p.isSugar()){
			premium=premium+(premium *0.01);			
		}
		if(p.isOverweight()){
			premium=premium+(premium *0.01);			
		}
		
		return premium;		
	}
	
	private double getPremiumByHabits(Person p){
		
		if(p.isExercise()){
			premium=premium-(basepremium *0.03);			
		}
		if(p.isSmoking()){
			premium=premium+(premium *0.03);			
		}
		if(p.isAlochol()){
			premium=premium+(premium *0.03);			
		}
		if(p.isDrugs()){
			premium=premium+(premium *0.03);			
		}
		
		return premium;			
	}

	
}
