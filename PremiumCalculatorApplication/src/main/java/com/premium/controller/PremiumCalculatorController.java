package com.premium.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.premium.service.Customer;
import com.premium.service.PremiumCalculatorService;

@RestController
public class PremiumCalculatorController {

    @Autowired
    private PremiumCalculatorService premiumService;
    
    @RequestMapping(method=RequestMethod.POST,value="/")
	public int generateQuote(@RequestBody Customer customer) {
		System.out.println("person"+ customer.toString());
		//personService.addPersonDetails(person);
		return (int) premiumService.getPremiumAmount(customer);
	}
}
